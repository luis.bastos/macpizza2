import React, { useState, useEffect } from 'react'
import { Container, Tab, Tabs } from 'react-bootstrap'
import styled from 'styled-components'

import TitlePage from '../../../components/titlePage'
import { getCategories, getProducts } from '../../../services/admin'
import { useDispatch, useSelector } from 'react-redux'
import { categoryList } from '../../../store/categories/category.actions'
export default () => {
    // const [categories, setCategories] = useState([])
    const dispatch = useDispatch()

    const categories = useSelector(state => state.category.categories)
    console.log('categories', categories)
    const [products, setProducts] = useState([])

    useEffect(() => {

        (async () => {
            dispatch(categoryList())
            const p = await getProducts();
            setProducts(p.data);
        })()

        //clear
        return () => () => { };
    }, [dispatch])
    const mountProducts = (cat) => {

        const prods = products.filter(item => item.category._id === cat._id)
        console.log(prods)
        return (
            <GridCards>

                {prods.length === 0
                    ? <div>Sem produtos</div>
                    : (
                        prods.map((prd, i) => (
                            // <CardProducts key={i} >
                            //     <Card.Img variant="top" src={prd.photo} />
                            //     <Card.Body>
                            //         <Card.Title>{prd.title}</Card.Title>
                            //         <Card.Text>
                            //             {prd.description}
                            //         </Card.Text>
                            //     </Card.Body>
                            // </CardProducts>
                            <li> { prd.title}</li>
                        ))
                    )
                }

            </GridCards >
        )
    }

    return (
        <Product>
            <TitlePage title="Produtos" sub="Conheça nossa Lista de Produtos" />

            <Container>
                <TabBox defaultActiveKey={1} className="produtos">
                    {categories.map((cat, i) => (
                        <Tab eventKey={i} key={i} title={cat.name}>
                            {mountProducts(cat)}
                        </Tab>
                    ))}
                </TabBox>
            </Container>
        </Product>
    )
}


const Product = styled.div`
    display:block;
    height: 700px;

    .produtos{
        margin-top: 20px;
    }
    .tab-content{
    }
    .tab-pane{
        background: pink;
        display: flex;
        div{
            display: grid;
            grid-gap: 10px;
            /* grid-template-columns: repeat(auto-fit, 250px); */
            width:100%;
        }
    }
`

const TabBox = styled(Tabs)`
    background: white;
`
const GridCards = styled.div`
    
`
