
import React from 'react'
import styled from 'styled-components'
import Carousel from 'react-bootstrap/Carousel'
import { Row, Col, Button } from 'react-bootstrap'
import BgBanner from '../../assets/images/queijo3.png'
import Img1 from '../../assets/images/cogumelos345.png'
import Img2 from '../../assets/images/napolitana3.png'
import Img3 from '../../assets/images/muçarela2.png'
import Img4 from '../../assets/images/palmito2.png'
import Img5 from '../../assets/images/camarão2.png'

const BannerHome = () => {
    return (
        <Banner>
            <div className="bg">
                <BannerItem>
                    <Carousel controls={false}>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img1} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Promoção</div>
                                    <h2 className="mb-4 title">Pizza de Cogumelos</h2>
                                    <div className="mb-4 desc">Pizza saborosa de Shitake e Shimeji</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img2} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Promoção</div>
                                    <h2 className="mb-4 title">Pizza Napolitana </h2>
                                    <div className="mb-4 desc">Pizza da casa Gourmet.</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img3} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Promoção</div>
                                    <h2 className="mb-4 title">Pizza de Muçarela </h2>
                                    <div className="mb-4 desc">Pizza saborosa e tradicional.</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img4} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Promoção</div>
                                    <h2 className="mb-4 title">Pizza de Palmito </h2>
                                    <div className="mb-4 desc">Nossos palmitos são os melhores do Brasil. Muito bem avaliada pelos clientes.</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img5} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Promoção</div>
                                    <h2 className="mb-4 title">Pizza de Camarão </h2>
                                    <div className="mb-4 desc">Camarões VG criados no nosso restaurante.</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                    </Carousel>
                </BannerItem>
            </div>
        </Banner>
    )
}

export default BannerHome


const Banner = styled.div`
    display: ${props => props.hidden === true ? 'none' : 'block'};
    height: 700px;
    width: 100%;
    background-image: url(${BgBanner});
    background-size: 100% 100%;
    overflow: hidden;

    .bg{
        background: #0003;
        height: 500px;
    }
`

const BannerItem = styled.div`
    color: #fff
`