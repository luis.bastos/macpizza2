import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import styled from 'styled-components'
import { AiFillFacebook, AiFillInstagram } from 'react-icons/ai'

export default () => {
    return (
        <Footer>
            <Container>
                <FooterInfo>
                    <Row>
                        <Col md={5}>
                            <div className="title">O que fazemos?</div>
                            <div className="aboutUs">
                                <p>As melhores pizzas do Brasil.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="title">Porque fazemos?</div>
                            <div className="menu">
                                <div className="text">
                                    <div> Porque queremos que vocês tenham muito prazer!</div>
                                </div>
                            </div>
                        </Col>
                        <Col md={3}>
                            <div className="title">Como fazemos?</div>
                            <div className="text">
                                    <div> Aí você está pedindo demais!</div>
                                </div>
                        </Col>
                    </Row>
                </FooterInfo>
                <Row>
                    <FooterCopy>
                </FooterCopy>
                </Row>
            </Container>
        </Footer>
    )
}



const Footer = styled.div`
    background: pink;
    padding: 10px 0;
    color: black;
    border-top: 2px solid  black;


`

const FooterInfo = styled.div`
    .title{
        font-size: 20px;
        font-weight: 600;
        padding: 10px 0;
        border-bottom: thin solid  black;
        margin-bottom: 10px;
        color: black;
    }
`

const FooterSocial = styled.div`
    cursor: pointer;
    width: 100%;
    border-bottom: 1px dotted #ccc;
    padding: 5px;
    svg {
        margin: 5px;
        font-size: 30px;
        :hover{
            color: #fac564;
        }
    }
`
const FooterCopy = styled.div`
    width: 100%;
    padding: 10px;
    text-align: center;
`

